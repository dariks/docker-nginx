FROM khs1994/nginx:1.15.6-stretch

RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN echo "Europe/Berlin" > /etc/timezone
RUN rm /etc/nginx/mime.types

RUN apt-get update && apt-get install curl bash libnginx-mod-http-dav-ext -y
